/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author kayab
 */
public class LabTest {
    
    public LabTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @Test
    public void switchTurn_O_X() {
        char turn = 'O';
        char result = Lab.switchTurn(turn);
        assertEquals('X', result);
    }
    
    @Test
    public void switchTurn_X_O() {
        char turn = 'X';
        char result = Lab.switchTurn(turn);
        assertEquals('O', result);
    }

    @Test
    public void checkRow_Row0_ture_ByX() {
        char[][] table = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char turn = 'X';
        int row = 0;
        boolean result = Lab.checkRow(table, turn, row);
        assertTrue(result);
    }
    
    @Test
    public void checkRow_Row1_ture_ByX() {
        char[][] table = {{'-', '-', '-'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char turn = 'X';
        int row = 1;
        boolean result = Lab.checkRow(table, turn, row);
        assertTrue(result);
    }
    
    @Test
    public void checkRow_Row2_ture_ByX() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        char turn = 'X';
        int row = 2;
        boolean result = Lab.checkRow(table, turn, row);
        assertTrue(result);
    }
    
    @Test
    public void checkRow_Row0_ture_ByO() {
        char[][] table = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char turn = 'O';
        int row = 0;
        boolean result = Lab.checkRow(table, turn, row);
        assertTrue(result);
    }
    
    @Test
    public void checkRow_Row1_ture_ByO() {
        char[][] table = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char turn = 'O';
        int row = 1;
        boolean result = Lab.checkRow(table, turn, row);
        assertTrue(result);
    }
    
    @Test
    public void checkRow_Row2_ture_ByO() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        char turn = 'O';
        int row = 2;
        boolean result = Lab.checkRow(table, turn, row);
        assertTrue(result);
    }
    
    
    @Test
    public void checkCol_Col0_ture_ByX() {
        char[][] table = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        char turn = 'X';
        int col = 0;
        boolean result = Lab.checkCol(table, turn, col);
        assertTrue(result);
    }
    
    @Test
    public void checkCol_Col1_ture_ByX() {
        char[][] table = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        char turn = 'X';
        int col = 1;
        boolean result = Lab.checkCol(table, turn, col);
        assertTrue(result);
    }
    
    @Test
    public void checkCol_Col2_ture_ByX() {
        char[][] table = {{'-', '-', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        char turn = 'X';
        int col = 2;
        boolean result = Lab.checkCol(table, turn, col);
        assertTrue(result);
    }
    
    @Test
    public void checkCol_Col0_ture_ByO() {
        char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char turn = 'X';
        int col = 0;
        boolean result = Lab.checkCol(table, turn, col);
        assertTrue(result);
    }
    
    @Test
    public void checkCol_Col1_ture_ByO() {
        char[][] table = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char turn = 'O';
        int col = 1;
        boolean result = Lab.checkCol(table, turn, col);
        assertTrue(result);
    }
    
    @Test
    public void checkCol_Col2_ture_ByO() {
        char[][] table = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        char turn = 'O';
        int col = 2;
        boolean result = Lab.checkCol(table, turn, col);
        assertTrue(result);
    }
    
    @Test
    public void checkX_X1_ture_ByX() {
        char[][] table = {{'X', '-', '-'}, {'-', 'X', '-'}, {'-', '-', 'X'}};
        char turn = 'X';
        int row = turn;
        int col = turn;
        boolean result = Lab.checkDia(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkX_X2_ture_ByX() {
        char[][] table = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        char turn = 'X';
        int row = turn;
        int col = turn;
        boolean result = Lab.checkDia(table, turn, row, col);
        assertTrue(result);
    }
     
    @Test
    public void checkX_X1_ture_ByO() {
        char[][] table = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char turn = 'O';
        int row = turn;
        int col = turn;
        boolean result = Lab.checkDia(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkX_X2_ture_ByO() {
        char[][] table = {{'-', '-', 'O'}, {'-', 'O', '-'}, {'O', '-', '-'}};
        char turn = 'O';
        int row = turn;
        int col = turn;
        boolean result = Lab.checkDia(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkRow0_Is_True_ByX() {
        char[][] table = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char turn = 'X';
        int col = 0;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkRow1_Is_True_ByX() {
        char[][] table = {{'-', '-', '-'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char turn = 'X';
        int col = 0;
        int row = 1;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkRow2_Is_True_ByX() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        char turn = 'X';
        int col = 0;
        int row = 2;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkRow0_Is_True_ByO() {
        char[][] table = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char turn = 'O';
        int col = 0;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkRow1_Is_True_ByO() {
        char[][] table = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char turn = 'O';
        int col = 0;
        int row = 1;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkRow2_Is_True_ByO() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        char turn = 'O';
        int col = 0;
        int row = 2;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkCol0_Is_True_ByX() {
        char[][] table = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        char turn = 'X';
        int col = 0;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkCol1_Is_True_ByX() {
        char[][] table = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        char turn = 'X';
        int col = 1;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkCol2_Is_True_ByX() {
        char[][] table = {{'-', '-', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        char turn = 'X';
        int col = 2;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }

    @Test
    public void checkWin_checkCol0_Is_True_ByO() {
        char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char turn = 'O';
        int col = 0;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkCol1_Is_True_ByO() {
        char[][] table = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char turn = 'O';
        int col = 1;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkCol2_Is_True_ByO() {
        char[][] table = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        char turn = 'X';
        int col = 2;
        int row = 0;
        boolean result = Lab.checkWin(table, turn, row, col);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkX1_Is_True_ByX() {
        Lab.table[0][2] = 'X';
        Lab.table[1][1] = 'X';
        Lab.table[2][0] = 'X';
        Lab.row = 0;
        Lab.col = 2;
        boolean result = Lab.checkWin(Lab.table, 'X', Lab.col, Lab.row);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkX2_Is_True_ByX() {
        Lab.table[0][0] = 'X';
        Lab.table[1][1] = 'X';
        Lab.table[2][2] = 'X';
        Lab.row = 0;
        Lab.col = 2;
        boolean result = Lab.checkWin(Lab.table, 'X', Lab.col, Lab.row);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkX1_Is_True_ByO() {
        Lab.table[0][2] = 'O';
        Lab.table[1][1] = 'O';
        Lab.table[2][0] = 'O';
        Lab.row = 0;
        Lab.col = 2;
        boolean result = Lab.checkWin(Lab.table, 'O', Lab.col, Lab.row);
        assertTrue(result);
    }
    
    @Test
    public void checkWin_checkX2_Is_True_ByO() {
        Lab.table[0][0] = 'O';
        Lab.table[1][1] = 'O';
        Lab.table[2][2] = 'O';
        Lab.row = 0;
        Lab.col = 2;
        boolean result = Lab.checkWin(Lab.table, 'O', Lab.col, Lab.row);
        assertTrue(result);
    }

    
}
