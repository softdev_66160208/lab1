/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.util.Scanner;

/**
 *
 * @author kayab
 */
public class Lab {

    public static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    public static char turn = '0';
    public static int row;
    public static int col;
    public static Scanner rcinput = new Scanner(System.in);
    public static int count = 0;

    public static void main(String[] args) {
        printWelcomeOX();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (isFinish()) {
                showTable();
                showResult();
                break;
            }
            turn = switchTurn(turn);
        }
        showTable();

    }

    public static void printWelcomeOX() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + turn);
    }

    public static void inputRowCol() {
        while (true) {
            System.out.println("Please input Row (1-3) and Col (1-3):");
            row = rcinput.nextInt() - 1;
            col = rcinput.nextInt() - 1;
            if (row >= 0 && row < 3 && col >= 0 && col < 3 && table[row][col] == '-') {
                table[row][col] = turn;
                count++;
                break;
            } else {
                System.out.println("Don't Cheat , Try Again!");
            }
        }
    }

    public static char switchTurn(char turn) {
        if (turn == 'X') {
            return 'O';
        } else {
            return 'X';
        }
    }

    public static boolean isFinish() {
        if (checkWin(table, turn, row, col)) {
            return true;
        }
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    public static boolean checkWin(char[][] table, char turn, int row, int col) {
        if (checkRow(table, turn, row)) {
            return true;
        }
        if (checkCol(table, turn, col)) {
            return true;
        }
        if (checkDia(table, turn, row, col)) {
            return true;
        }
        return false;
    }

    public static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    public static boolean checkRow(char[][] table, char turn, int row) {
        if (table[row][0] == turn && table[row][1] == turn && table[row][2] == turn) {
            return true;
        }
        return false;
    }

    public static boolean checkCol(char[][] table, char turn, int col) {
        if (table[0][col] == turn && table[1][col] == turn && table[2][col] == turn) {
            return true;
        }
        return false;
    }

    public static boolean checkDia(char[][] table, char turn, int row, int col) {
        if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
            return true;
        }
        if (table[0][2] == turn && table[1][1] == turn && table[2][0] == turn) {
            return true;
        }
        return false;
    }

    public static void showResult() {
        if (checkWin(table, turn, row, col)) {
            System.out.println(turn + " Win!");
        }
        if (checkDraw()) {
            System.out.println("Draw!");
        }
    }

}
